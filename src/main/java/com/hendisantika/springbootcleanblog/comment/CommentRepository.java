package com.hendisantika.springbootcleanblog.comment;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-clean-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 27/06/18
 * Time: 07.56
 * To change this template use File | Settings | File Templates.
 */
public interface CommentRepository extends JpaRepository<Comment, Long> {
}
