package com.hendisantika.springbootcleanblog.post;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.internal.verification.VerificationModeFactory.times;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-clean-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 08/07/18
 * Time: 07.12
 * To change this template use File | Settings | File Templates.
 */
@RunWith(MockitoJUnitRunner.class)
public class PostServiceTest {


    @Mock
    private PostRepository postRepository;

    private PostService postService;

    @Before
    public void setup() {
        postService = new PostService(postRepository);
    }


    @Test
    public void createPost() {
        final Post post = new Post("post title", "post content",
                "code", PostStatus.Y, null, null);
        given(postRepository.save(any(Post.class)))
                .willReturn(post);

        final Post result = postRepository.save(post);
        assertThat(result.getTitle()).isEqualTo("post title");
        assertThat(result.getContent()).isEqualTo("post content");
        assertThat(result.getCode()).isEqualTo("code");
        assertThat(result.getStatus()).isEqualTo(PostStatus.Y);
    }

    @Test
    public void updatePost() {
        final Post post = new Post("post title", "post content",
                "code", PostStatus.Y, null, null);
        given(postRepository.findByIdAndStatus(any(), any()))
                .willReturn(post);

        final Post result = postService.updatePost(1L, post);
        assertThat(result.getTitle()).isEqualTo("post title");
        assertThat(result.getContent()).isEqualTo("post content");
        assertThat(result.getCode()).isEqualTo("code");
        assertThat(result.getStatus()).isEqualTo(PostStatus.Y);

    }

    @Test
    public void deletePost() {
        final Post post = new Post("post title", "post content",
                "code", PostStatus.Y, null, null);
        given(postRepository.findByIdAndStatus(any(), any()))
                .willReturn(post);
        postService.deletePost(1L);
        verify(postRepository, times(1))
                .findByIdAndStatus(1L, PostStatus.Y);
    }

    @Test
    public void findByIdAndStatus() {
        final Post post = new Post("post title", "post content",
                "code", PostStatus.Y, null, null);
        given(postRepository.findByIdAndStatus(any(), any()))
                .willReturn(post);

        final Post result = postService.findByIdAndStatus(1L, PostStatus.Y);
        assertThat(result.getTitle()).isEqualTo("post title");
        assertThat(result.getContent()).isEqualTo("post content");
        assertThat(result.getCode()).isEqualTo("code");
        assertThat(result.getStatus()).isEqualTo(PostStatus.Y);
    }

}