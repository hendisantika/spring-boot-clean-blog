package com.hendisantika.springbootcleanblog.post;

import com.hendisantika.springbootcleanblog.exception.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-clean-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 25/06/18
 * Time: 07.11
 * To change this template use File | Settings | File Templates.
 */
@Service
@Transactional
@RequiredArgsConstructor
public class PostService {
    private final PostRepository postRepository;

    public Post createPost(Post post) {
        post.setRegDate(LocalDateTime.now());
        return postRepository.save(post);
    }

    public Post updatePost(Long id, Post post) {
        Post oldPost = postRepository.findByIdAndStatus(id, PostStatus.Y);
        if (oldPost == null) {
            throw new NotFoundException(id + " not found");
        }

        oldPost.setContent(post.getContent());
        oldPost.setCode(post.getCode());
        oldPost.setTitle(post.getTitle());
        return oldPost;
    }

    public void deletePost(Long id) {
        Post oldPost = postRepository.findByIdAndStatus(id, PostStatus.Y);
        if (oldPost == null) {
            throw new NotFoundException(id + " not found");
        }
        oldPost.setStatus(PostStatus.N);
    }

    @Transactional(readOnly = true)
    public Post findByIdAndStatus(Long id, PostStatus status) {
        Post post = postRepository.findByIdAndStatus(id, status);
        if (post == null) {
            throw new NotFoundException(id + " not found");
        }
        return post;
    }
}
