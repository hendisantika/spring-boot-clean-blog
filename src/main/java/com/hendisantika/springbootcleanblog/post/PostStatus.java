package com.hendisantika.springbootcleanblog.post;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-clean-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 24/06/18
 * Time: 07.45
 * To change this template use File | Settings | File Templates.
 */
public enum PostStatus {
    Y,
    N
}
