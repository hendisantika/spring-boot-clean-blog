package com.hendisantika.springbootcleanblog.config;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-clean-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 25/06/18
 * Time: 07.19
 * To change this template use File | Settings | File Templates.
 */
public enum Section {
    HOME("Home"),
    POST("Post"),
    CATEGORY("Category");

    private String value;

    Section(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
