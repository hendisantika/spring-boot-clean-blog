package com.hendisantika.springbootcleanblog.comment;

import com.hendisantika.springbootcleanblog.junit.MockitoJsonJUnitRunner;
import com.hendisantika.springbootcleanblog.post.Post;
import com.hendisantika.springbootcleanblog.user.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.json.JacksonTester;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.internal.verification.VerificationModeFactory.times;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-clean-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 07/07/18
 * Time: 07.40
 * To change this template use File | Settings | File Templates.
 */
@RunWith(MockitoJsonJUnitRunner.class)
public class CommentServiceTest {

    private JacksonTester<Comment> json;

    @Mock
    private CommentRepository commentRepository;

    private CommentService commentService;

    @Before
    public void setup() {
        commentService = new CommentService(commentRepository);
    }

    @Test
    public void createComment() throws Exception {
        final Comment comment = new Comment("good post", new Post(null, null, null, null, null, null), new User("test", "test", "test", "test"));
        comment.setId(1L);
        given(commentRepository.save(any(Comment.class)))
                .willReturn(comment);
        final Comment result = commentService.createComment(comment);
        assertThat(this.json.write(result))
                .isEqualToJson("createcomment.json");
    }

    @Test
    public void deleteComment() {
        doNothing().when(commentRepository).delete(any(Comment.class));
        commentRepository.delete(1L);
        verify(commentRepository, times(1)).delete(1L);
    }

}