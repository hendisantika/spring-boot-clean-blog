package com.hendisantika.springbootcleanblog.comment;

import com.hendisantika.springbootcleanblog.category.Category;
import com.hendisantika.springbootcleanblog.post.Post;
import com.hendisantika.springbootcleanblog.post.PostStatus;
import com.hendisantika.springbootcleanblog.user.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Java6Assertions.assertThat;


/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-clean-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 08/07/18
 * Time: 06.52
 * To change this template use File | Settings | File Templates.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class CommentRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private CommentRepository commentRepository;

    @Test
    public void findOneTest() {
        final User user = this.testEntityManager.persist(new User("test",
                "test",
                "set",
                "https://test"));

        final Category category = this.testEntityManager.persist(new Category("spring"));

        final Post post = this.testEntityManager.persist(new Post("test title", "test content", "test content", PostStatus.Y, category, user));
        final Comment persist = this.testEntityManager.persist(new Comment("test commnet", post, user));
        final Comment comment = commentRepository.findOne(persist.getId());
        assertThat(comment.getContent()).isEqualTo(comment.getContent());
    }
}