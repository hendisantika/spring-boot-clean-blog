package com.hendisantika.springbootcleanblog.category;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-clean-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 26/06/18
 * Time: 07.31
 * To change this template use File | Settings | File Templates.
 */
@Data
public class CategoryDto {
    private Long id;

    @NotBlank
    private String name;
}