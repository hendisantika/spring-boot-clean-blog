package com.hendisantika.springbootcleanblog.junit;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.runner.Description;
import org.junit.runner.manipulation.Filter;
import org.junit.runner.manipulation.NoTestsRemainException;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.InitializationError;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.runners.RunnerImpl;
import org.mockito.internal.runners.util.FrameworkUsageValidator;
import org.springframework.boot.test.json.JacksonTester;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-clean-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 04/07/18
 * Time: 07.05
 * To change this template use File | Settings | File Templates.
 */
public class JsonRunner implements RunnerImpl {
    BlockJUnit4ClassRunner runner;

    public JsonRunner(Class<?> klass) throws InitializationError {
        this.runner = new BlockJUnit4ClassRunner(klass) {
            @Override
            protected Object createTest() throws Exception {
                Object test = super.createTest();
                ObjectMapper objectMapper = new ObjectMapper();
                JacksonTester.initFields(test, objectMapper);
                MockitoAnnotations.initMocks(test);
                return test;
            }
        };
    }

    public void run(RunNotifier notifier) {
        notifier.addListener(new FrameworkUsageValidator(notifier));
        runner.run(notifier);
    }

    public Description getDescription() {
        return runner.getDescription();
    }

    public void filter(Filter filter) throws NoTestsRemainException {
        runner.filter(filter);
    }
}