package com.hendisantika.springbootcleanblog.post;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-clean-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 25/06/18
 * Time: 07.09
 * To change this template use File | Settings | File Templates.
 */
@Data
public class PostDto {

    private Long id;
    @NotBlank
    private String title;
    @NotBlank
    private String content;

    private String code;

    @NotNull
    private Long categoryId;

    private String categoryName;
}
