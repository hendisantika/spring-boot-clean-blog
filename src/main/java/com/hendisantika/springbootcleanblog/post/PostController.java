package com.hendisantika.springbootcleanblog.post;

import com.hendisantika.springbootcleanblog.category.Category;
import com.hendisantika.springbootcleanblog.category.CategoryService;
import com.hendisantika.springbootcleanblog.comment.CommentDto;
import com.hendisantika.springbootcleanblog.config.Navigation;
import com.hendisantika.springbootcleanblog.config.Section;
import com.hendisantika.springbootcleanblog.exception.NotFoundException;
import com.hendisantika.springbootcleanblog.user.User;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-clean-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 25/06/18
 * Time: 07.12
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequiredArgsConstructor
@RequestMapping("/posts")
@Navigation(Section.POST)
public class PostController {

    private final PostService postService;
    private final CategoryService categoryService;

    @ModelAttribute("categories")
    public List<Category> categories() {
        return categoryService.findAll();
    }

    @GetMapping("/{id}")
    public String findByPost(@PathVariable Long id, Model model, @ModelAttribute CommentDto commentDto) {
        Post post = postService.findByIdAndStatus(id, PostStatus.Y);
        if (post == null) {
            throw new NotFoundException(id + " not found");
        }
        model.addAttribute("post", post);
        return "post/post";
    }

    @GetMapping("/new")
    public String newPost(PostDto postDto) {
        return "post/new";
    }

    @GetMapping("/edit/{id}")
    public String editPost(@PathVariable Long id, Model model) {
        Post post = postService.findByIdAndStatus(id, PostStatus.Y);
        if (post == null) {
            throw new NotFoundException(id + " not found");
        }
        PostDto createPost = new PostDto();
        createPost.setCategoryId(post.getCategory().getId());
        createPost.setCategoryName(post.getCategory().getName());
        createPost.setTitle(post.getTitle());
        createPost.setCode(post.getCode());
        createPost.setContent(post.getContent());
        createPost.setId(id);
        model.addAttribute("editPost", createPost);
        return "post/edit";
    }

    @PostMapping
    public String createPost(@ModelAttribute @Valid PostDto createPost, BindingResult bindingResult, Model model, @AuthenticationPrincipal User user) {
        if (bindingResult.hasErrors()) {
            return "post/new";
        }
        Post post = new Post(createPost.getTitle(),
                createPost.getContent(),
                createPost.getCode(),
                PostStatus.Y,
                new Category(createPost.getCategoryId()),
                user);
        Post newPost = postService.createPost(post);
        model.addAttribute("post", newPost);
        return "redirect:/posts/" + newPost.getId();
    }

    @PostMapping("/{id}/edit")
    public String modifyPost(@PathVariable Long id, @ModelAttribute("editPost") @Valid PostDto createPost, BindingResult bindingResult, @AuthenticationPrincipal User user) {
        if (bindingResult.hasErrors()) {
            return "post/edit";
        }
        postService.updatePost(id, new Post(
                createPost.getTitle(),
                createPost.getContent(),
                createPost.getCode(),
                PostStatus.Y,
                new Category(createPost.getCategoryId()),
                user
        ));
        return "redirect:/posts/" + id;
    }

    @PostMapping("{id}/delete")
    public String deletePost(@PathVariable Long id) {
        postService.deletePost(id);
        return "redirect:/#/";
    }
}