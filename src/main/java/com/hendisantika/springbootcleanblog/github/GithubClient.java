package com.hendisantika.springbootcleanblog.github;

import com.hendisantika.springbootcleanblog.exception.NotFoundException;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-clean-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 24/06/18
 * Time: 07.58
 * To change this template use File | Settings | File Templates.
 */

@Service
public class GithubClient {

    private final static String GIT_HUB_URL = "https://api.github.com";
    private final RestTemplate restTemplate;

    public GithubClient(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    @Cacheable("github.user")
    public GithubUser getUser(String githubId) {
        return invoke(createRequestEntity(
                String.format(GIT_HUB_URL + "/users/%s", githubId)), GithubUser.class).getBody();
    }

    private <T> ResponseEntity<T> invoke(RequestEntity<?> request, Class<T> type) {
        try {
            return this.restTemplate.exchange(request, type);
        } catch (HttpClientErrorException ex) {
            if (ex.getStatusCode() == HttpStatus.NOT_FOUND) {
                throw new NotFoundException("not found");
            }
            throw ex;
        }
    }

    private RequestEntity<?> createRequestEntity(String url) {
        try {
            return RequestEntity.get(new URI(url))
                    .accept(MediaType.APPLICATION_JSON).build();
        } catch (URISyntaxException ex) {
            throw new IllegalStateException("Invalid URL " + url, ex);
        }
    }
}
