package com.hendisantika.springbootcleanblog.comment;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-clean-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 27/06/18
 * Time: 07.55
 * To change this template use File | Settings | File Templates.
 */
@Data
public class CommentDto {

    @NotNull
    private Long postId;

    @NotBlank
    private String content;
}