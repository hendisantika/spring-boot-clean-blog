package com.hendisantika.springbootcleanblog.post;

import com.hendisantika.springbootcleanblog.category.Category;
import com.hendisantika.springbootcleanblog.user.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Java6Assertions.assertThat;


/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-clean-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 08/07/18
 * Time: 07.14
 * To change this template use File | Settings | File Templates.
 */

@RunWith(SpringRunner.class)
@DataJpaTest
public class PostRepositoryTest {
    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private PostRepository postRepository;


    @Test
    public void findByIdAndStatus() {
        final User user = this.testEntityManager.persist(new User("test",
                "test",
                "set",
                "https://test"));

        final Category category = this.testEntityManager.persist(new Category("spring"));

        final Post persist = this.testEntityManager.persist(new Post("test title", "test content", "test content", PostStatus.Y, category, user));
        Post post = this.postRepository.findByIdAndStatus(persist.getId(), PostStatus.Y);
        assertThat(post.getTitle()).isEqualTo("test title");
        assertThat(post.getContent()).isEqualTo("test content");
        assertThat(post.getCode()).isEqualTo("test content");
        assertThat(post.getStatus()).isEqualTo(PostStatus.Y);
    }
}