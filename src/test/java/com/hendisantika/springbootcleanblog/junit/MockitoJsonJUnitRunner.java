package com.hendisantika.springbootcleanblog.junit;

import org.junit.runner.Description;
import org.junit.runner.Runner;
import org.junit.runner.notification.RunNotifier;
import org.mockito.internal.runners.util.RunnerProvider;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-clean-blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 04/07/18
 * Time: 07.06
 * To change this template use File | Settings | File Templates.
 */
public class MockitoJsonJUnitRunner extends Runner {

    private final RunnerImpl runner;

    public MockitoJsonJUnitRunner(Class<?> clazz) throws Exception {
        this.runner = new RunnerProvider().newInstance("me.wonwoo.junit.JsonRunner", clazz);
    }

    @Override
    public void run(final RunNotifier notifier) {
        runner.run(notifier);
    }

    @Override
    public Description getDescription() {
        return runner.getDescription();
    }
}